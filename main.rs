#[macro_use]
extern crate allegro;
extern crate allegro_font;
extern crate allegro_primitives;
extern crate allegro_sys;

#[macro_use]
extern crate derive_new;


use FASIW::Control::*;
use FASIW::Interface::*;
use allegro::*;
use allegro_font::*;
use allegro_primitives::*;
use allegro_sys::*;
const CLICK: i32 = 1;

mod FASIW {
	pub mod Control {
		pub static mut x_mouse_click: f32 = -100.0;
		pub static mut  y_mouse_click: f32 = -100.0;
		pub static mut  mouse_status: i32 = 0;
		pub static mut  number_click: i32 = 0;

		pub fn x_writter(ch1: f32) {
        unsafe { x_mouse_click = ch1; }
	    }
	    pub fn y_writter(ch2: f32) {
	        unsafe { y_mouse_click = ch2; }
	    }
	    pub fn get_x() -> f32 {
	        unsafe { let x_ret = x_mouse_click;
	           	   x_ret 				      }
	    }
	    pub fn get_y() -> f32 {
	        unsafe { let y_ret = y_mouse_click;
	               y_ret 					  }
	    }
	    pub fn change_mouse_status(changer_number: i32) {
	        unsafe { mouse_status = changer_number; }
	    }
	    pub fn return_mouse_status() -> i32 {
	        unsafe { mouse_status }
	    }
	    pub fn click_for_numbers() {
	        unsafe { number_click = number_click + 1; }
	    }
	    pub fn return_click_mouse() -> i32 {
	        unsafe { number_click }
	    }
	    pub fn set_click_for_numbers(num11: i32) {
	        unsafe { number_click = num11; }
	    }
	    pub fn init_mouse(statusvar: i32, xvar: &i32, yvar: &i32) {
	    	click_for_numbers();
            change_mouse_status(statusvar);
            let x = *xvar;
            let y = *yvar;
            x_writter((x) as f32);
            y_writter((y) as f32);
		}
	}

	pub mod Interface {

		use FASIW::Control::*;
		use allegro::*;
		use allegro_font::*;
		use allegro_primitives::*;
		use allegro_sys::*;

		pub trait ActionWith<'a> {
		    fn draw(&self);
		    fn click(&mut self, i32) -> i32;
		    fn ChangeColor(&mut self, u8, u8, u8, u8);
		    fn click_inside<T>(&self, T) -> bool;
		    fn ChangeText(&mut self, textfunction: &'a str);
		    fn ChangeTextColor(&mut self, r: u8 , g: u8, b: u8, a: u8);
		}

		#[derive(new)]
		pub struct Button<'a> {
		    x: f32,
		    y: f32,
		    w: f32,
		    h: f32,
		    alo: i64,
		    rgba: Color,
		    q: &'a PrimitivesAddon,
		    #[new(value = "Color::from_rgba(0, 0, 0, 255)")]
		    textcolor: Color,
		    text: &'a str,
		}

		impl<'a> ActionWith<'a> for Button<'a> {

			fn click_inside<T>(&self, obj: T) -> bool {
			if get_x() > self.x && 
		       get_x() < self.w && 
		       get_y() > self.y && 
		       get_y() < self.h {
		       	true
		       } else { false }
			}

		    fn draw(&self) {
		        self.q
		            .draw_filled_rectangle(self.x, self.y, self.w, self.h, self.rgba);
		    }

		    fn click(&mut self, returN: i32) -> i32 {
		        if self.click_inside(&self) == true
		        {
		            1
		        } else {
		            2
		        }
		    }

		    fn ChangeColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		        self.rgba = Color::from_rgba(r, g, b, a);
		    }

		    fn ChangeText(&mut self, textfunction: &'a str) {
		    	self.text = textfunction;
		    }

		    fn ChangeTextColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		    	self.textcolor = Color::from_rgba(r, g, b, a);
		    }
		}

		#[derive(new)]
		pub struct CheckBox<'a> {
		    x: f32,
		    y: f32,
		    w: f32,
		    h: f32,
		    alo: i64,
		    rgba: Color,
		    q: &'a PrimitivesAddon,
		    #[new(value = "0")]
		    checkbox_clicks: i32,
		    #[new(value = "Color::from_rgba(0, 0, 0, 255)")]
		    textcolor: Color,
		    text: &'a str,
		}

		impl<'a> ActionWith<'a> for CheckBox<'a> {
			fn click_inside<T>(&self, obj: T) -> bool {
			if get_x() > self.x && 
		       get_x() < self.w && 
		       get_y() > self.y && 
		       get_y() < self.h {
		       	true
		       } else { false }
			}

		    fn draw(&self) {
		        self.q
		            .draw_filled_rectangle(self.x, self.y, self.w, self.h, self.rgba);
		    }

		    fn click(&mut self, returN: i32) -> i32 {
		        unsafe {
		            if self.click_inside(&self) == true
		            {
		                self.checkbox_clicks = self.checkbox_clicks + 1;
		                if (self.checkbox_clicks % 2) == 0 {
		                    return 1;
		                } else {
		                    return 2;
		                }
		            }
		            return 0;
		        }
		    }

		    fn ChangeColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		        self.rgba = Color::from_rgba(r, g, b, a);
		    }

		    fn ChangeText(&mut self, textfunction: &'a str) {
		    	self.text = textfunction;
		    }

		    fn ChangeTextColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		    	self.textcolor = Color::from_rgba(r, g, b, a);
		    }
		}

		#[derive(new)]
		pub struct Texter<'a> {
		    core: &'a Core,
		    x: f32,
		    y: f32,
		    rgba: Color,
		    text: &'a str,
		    size: i32,
		}

		impl<'a> Texter<'a> {
		    pub fn draw(&mut self, ff: &Font) {
		        self.core.draw_text(
		            &ff,
		            self.rgba,
		            self.x,
		            self.y,
		            FontAlign::Centre,
		            &self.text,
		        );
		    }
		}

		#[derive(new)]
		pub struct InputText<'a> {
			core: &'a Core,
		    x: f32,
		    y: f32,
		    w: f32,
		    h: f32,
		    rgba: Color,
		    text: &'a str,
		    size: i32,
		    border: f32,
		    q: &'a PrimitivesAddon,
		    #[new(value = "0")]
		    moveindex: usize,
		    #[new(value = "Color::from_rgba(0, 0, 0, 255)")]
		    textcolor: Color,
		}

		impl <'a>InputText<'a>  {

			pub fn click_inside<T>(&self, obj: T) -> bool {
			if get_x() > self.x && 
		       get_x() < self.w && 
		       get_y() > self.y && 
		       get_y() < self.h {
		       	true
		       } else { false }
			}

		    pub fn draw(&mut self, ff: &Font) {
		        self.q
		            .draw_filled_rectangle(self.x, self.y, self.w, self.h, self.rgba);

		            let mut textcord1 = (self.w - self.x)/2.0 + self.x;
		            let mut textcord2 = (self.h - self.y)/2.0 + self.y;

		            self.core.draw_text(
		            &ff,
		            self.textcolor,
		            textcord1,
		            textcord2,
		            FontAlign::Centre,
		            &self.text,
		        );
		    }

		    pub fn click(&mut self, returN: i32) -> i32 {
		        unsafe {
		            if self.click_inside(&self) == true
		            {
		                return 1;
		            } else {
		                return 0;
		            }
		        }
		    }

		    pub fn ChangeColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		        self.rgba = Color::from_rgba(r, g, b, a);
		    }

		    pub fn ChangeText(&mut self, textfunction: &'a str) {
		    	self.text = textfunction;
		    }

		    pub fn ChangeTextColor(&mut self, r: u8 , g: u8, b: u8, a: u8) {
		    	self.textcolor = Color::from_rgba(r, g, b, a);
		    }
		}
	}
}



allegro_main!
{
    let core = Core::init().unwrap();
    let font_addon = FontAddon::init(&core).unwrap();
    let mut  primit1 = PrimitivesAddon::init(&core).unwrap();
    let display = Display::new(&core, 800, 400).unwrap();
    let timer = Timer::new(&core, 1.0 / 60.0).unwrap();
    let font = Font::new_builtin(&font_addon).unwrap();
    
    core.install_mouse().unwrap();
    core.install_keyboard().unwrap();

    let queue = EventQueue::new(&core).unwrap();

    queue.register_event_source(display.get_event_source());
    queue.register_event_source(timer.get_event_source());
    queue.register_event_source(core.get_mouse_event_source().unwrap());
    queue.register_event_source(core.get_keyboard_event_source().unwrap());

let mut chb1 = CheckBox::new(200.0, 200.0, 250.0, 250.0, 25, Color::from_rgba(10,10,10,255), &primit1, "000");
let mut chb2 = CheckBox::new(300.0, 300.0, 400.0, 400.0, 25, Color::from_rgba(10,10,10,255), &primit1, "1234Э");
let mut Bu1 = Button::new(200.0, 100.0, 350.0, 250.0, 25, Color::from_rgba(0,0,0,255), &primit1, "123");
let mut text1 = Texter::new(&core, 250.0 ,250.0 , Color::from_rgba(255, 255, 255, 255), "nigga", 15 );
let mut ipt = InputText::new(&core, 10.0 , 10.0, 150.0 , 150.0, Color::from_rgba(180, 180, 180, 255), "bed", 15 ,  15.0, &primit1);


    let mut redraw = true;
    timer.start();
    'exit: loop
    {

        if redraw && queue.is_empty()
        {
            core.clear_to_color(Color::from_rgb_f(255.0,255.0,255.0));
            core.draw_text(&font,Color::from_rgba(255, 255, 255, 255), 100.0,100.0, FontAlign::Centre, "123");
            ipt.draw(&font);
            text1.draw(&font);
            Bu1.draw();
            core.flip_display();
            redraw = false;

        }

        match queue.wait_for_event()
        {
            DisplayClose{..} => break 'exit,
            TimerTick{..} => redraw = true,
            MouseAxes{x, y , ..} => {},
            MouseButtonUp{x,y, ..} => {change_mouse_status(1);  },
            MouseButtonDown{x,y, ..} => {
                init_mouse(0,&x,&y);

                ipt.click(1);
                },
            MouseLeaveDisplay{..} => {},
            KeyChar{keycode, unichar, ..} => {let mut k = unichar; println!("{}", k);},
            _ => (),
        }
    }
}
